UFSCTeX
=======

ufscThesis é uma classe LaTeX desenvolvida por
Roberto Simoni e Carlos Rocha,
que formata dissertações de acordo com as normas
estabelecidas pela BU-UFSC.

Este repositório modulariza o código produzido por eles,
e faz algumas modificações triviais
visando facilitar o uso num ambiente Linux.

O código original pode ser encontrado em
http://sbu.paginas.ufsc.br/files/2011/03/modelo.zip.


Uso
===

Um template que utiliza o ufscThesis está disponível em
https://github.com/royertiago/ufsctex-template.

É necessário estar instalado abnTeX, versão 0.8.2 ou posterior.
Ele está disponível junto do .zip no link acima;
também é possível instalá-lo usando

    apt-get install abntex


Licença
=======

Ambos os arquivos são distribuídos em licença dupla
LPPL (Latex Project Public License) /
GPL (GNU General Public License).
Consulte cada arquivo para mais detalhes.

(Em particular, a licença LPPL obriga versões alteradas
a possuírem um novo nome de arquivo;
por isso, renomeei os arquivos para ufsctex.)
